﻿using cadastromi.Models.Configuracoes;
using cadastromi.Models.entidades;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace cadastromi.Data
{
    public class CadastroMiContext : DbContext
    {
        public CadastroMiContext(DbContextOptions<CadastroMiContext> options) : base(options) { }

        public DbSet<Usuario> Usuarios { get; set; }

        public DbSet<Produto> Produtos { get; set; }






        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new UsuarioConfiguracao());

            modelBuilder.ApplyConfiguration(new ProdutoConfiguracao());



        }


    }
}
