﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using cadastromi.Data;
using cadastromi.Models.entidades;
using cadastromi.ViewModels.ProdutosViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace cadastromi.Controllers.V1
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ProdutosController : CadastroMiController
    {
        public ProdutosController(CadastroMiContext context) : base(context)
        {
        }

        [HttpGet]
        [Route("v1/[action]")]
        public IActionResult Get()
        {
            var produtos = db.Produtos.Where(a => a.UserId == UsuarioLogado.Id).OrderBy(a => a.DataEntrega).Select(p => new
            {
                p.Id,
                DataImportacao = p.DataImportacao.ToShortDateString(),
                p.Quantidade,
                DataEntrega = p.DataEntrega.Value.ToShortDateString(),
                total = p.ValorUnitario + p.Quantidade

            }).ToList();

            return Json(produtos);
        }


        [HttpGet("{id}")]
        [Route("v1/[action]/{id}")]
        public IActionResult Get([FromRoute] int id)
        {
            var produto = db.Produtos.Where(a => a.UserId == UsuarioLogado.Id && a.Id == id).OrderByDescending(a => a.DataEntrega).Select(p => new
            {
                p.Id,
                DataImportacao = p.DataImportacao.ToShortDateString(),
                p.Quantidade,
                DataEntrega = p.DataEntrega.Value.ToShortDateString(),
                total = p.ValorUnitario + p.Quantidade

            }).ToList();


            if (produto == null)
                return NotFound();

            return Json(produto);

        }

        // POST api/<ProdutosController>
        [HttpPost]
        [Route("v1/[action]")]
        public async Task<IActionResult> Post([FromForm] EnviaArquivoViewModel model)
        {
            if (ModelState.IsValid)
            {


                try
                {


                    using (var stream = new MemoryStream())
                    {
                        await model.Arquivo.CopyToAsync(stream);

                        using (var package = new ExcelPackage(stream))
                        {

                            ExcelWorksheet worksheet = package.Workbook.Worksheets.FirstOrDefault();

                            var end = worksheet.Dimension.End;




                            for (int row = 2; row <= end.Row; row++)
                            {
                                model.ProdutosInformado.Add(new EnviaArquivoViewModel.ProdutosInformados
                                {

                                    DataEntrega = worksheet.Cells[row, 1].Text == string.Empty ? "Não informado" : worksheet.Cells[row, 1].Value.ToString().Trim(),
                                    Nome = worksheet.Cells[row, 2].Text == string.Empty ? "Não informado" : worksheet.Cells[row, 2].Value.ToString().Trim(),
                                    Quantidade = worksheet.Cells[row, 3].Text == string.Empty ? "Não informado" : worksheet.Cells[row, 3].Value.ToString().Trim(),
                                    ValorUnitario = worksheet.Cells[row, 4].Text == string.Empty ? "Não informado" : worksheet.Cells[row, 4].Value.ToString().Trim(),

                                });
                            }



                            foreach (var item in model.ProdutosInformado)
                            {

                                var produto = new Produto()
                                {
                                    Nome = item.Nome,
                                    DataEntrega = DateTime.Parse(item.DataEntrega),
                                    Quantidade = Convert.ToInt32(item.Quantidade),
                                    ValorUnitario = Convert.ToDecimal(item.ValorUnitario),
                                    UserId = UsuarioLogado.Id,

                                };

                                db.Produtos.Add(produto);




                            }

                            db.SaveChanges();





                        }

                    }




                    return Ok();

                }
                catch (Exception e)
                {

                    return StatusCode(500, e.Message);
                }





            }

            return BadRequest(new { msgs = ModelState.Select(a => a.Value.Errors).ToArray() });



        }

    }
}
