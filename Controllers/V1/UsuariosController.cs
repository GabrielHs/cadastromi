﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using cadastromi.Data;
using cadastromi.Models.entidades;
using cadastromi.Utils;
using cadastromi.ViewModels.UsuariosViewModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace cadastromi.Controllers.V1
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsuariosController : CadastroMiController
    {
        public UsuariosController(CadastroMiContext context) : base(context)
        {
        }



        [HttpPost]
        [Route("v1/[action]")]
        public IActionResult Post([FromBody] RegistroViewModel model)
        {
            model.ValidaDados(this);


            if (ModelState.IsValid)
            {

                try
                {
                    using (var transaction = db.Database.BeginTransaction())
                    {


                        var usuarios = new Usuario();

                        usuarios.Login = model.Login.Trim().ToLower();

                        usuarios.Papel = string.IsNullOrEmpty(model.Papel) ? "Comum" : model.Papel;

                        var passwordHasher = new PasswordHasher<Usuario>();

                        var hassSenha = passwordHasher.HashPassword(usuarios, model.Senha);

                        usuarios.Senha = hassSenha;

                        db.Usuarios.Add(usuarios);

                        db.SaveChanges();


                        transaction.Commit();


                        return Ok();

                    }
                }
                catch (Exception e)
                {

                    return StatusCode(500, e.Message);
                }
            }


            return BadRequest(new { msgs = ModelState.Select(a => a.Value.Errors).ToArray() });


        }




        [HttpPost]
        [Route("v1/[action]")]
        public IActionResult Login([FromBody] LoginViewModel model)
        {
            model.ValidaDados(this);


            if (ModelState.IsValid)
            {

                try
                {


                    var usuario = db.Usuarios.FirstOrDefault(us => us.Login == model.Login.ToLower().Trim());

                    var token = Helper.GenerateToken(usuario);

                    var retorno = new
                    {
                        usuario,
                        jwt = token
                    };


                    return Ok(retorno);
                }
                catch (Exception e)
                {

                    return StatusCode(500, e.Message);
                }
            }


            return BadRequest(new { msgs = ModelState.Select(a => a.Value.Errors).ToArray() });


        }












    }
}
