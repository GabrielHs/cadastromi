﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using cadastromi.Data;
using cadastromi.Models.entidades;
using cadastromi.Utils;
using Microsoft.AspNetCore.Mvc;

namespace cadastromi.Controllers
{
    public class CadastroMiController : Controller
    {

        protected readonly CadastroMiContext db;

        public CadastroMiContext Db => db;

        private int? idUsuarioAtualCache;

        private string loginCache;

        public CadastroMiController(CadastroMiContext context)
        {
            this.db = context;
        }



        public Usuario UsuarioLogado
        {
            get
            {
                // Se o usuário atual já foi chamado e não mudou deste a última chamada
                if (idUsuarioAtualCache != null && !string.IsNullOrWhiteSpace(loginCache) && loginCache.ToLower() == HttpContext.User?.Identity.Name.ToLower())
                {

                    return db.Usuarios.Find(idUsuarioAtualCache);
                }

                // Busca o usuário a partir do banco
                var usuarioAtual = Helper.UsuarioLogado(db, Request.HttpContext);

                // Seta o id do usuário encontrado em cache (se houver)
                idUsuarioAtualCache = usuarioAtual?.Id;
                loginCache = usuarioAtual?.Login;

                // Retorna usuário encontrado
                return usuarioAtual;
            }
        }




    }
}
