﻿
using cadastromi.Data;
using cadastromi.Models.entidades;
using Microsoft.AspNetCore.Http;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace cadastromi.Utils
{
    public static class Helper
    {
        public static Usuario UsuarioLogado(CadastroMiContext db, HttpContext httpContext)
        {
            if (httpContext == null)
            {
                throw new ArgumentNullException($"O parâmetro {nameof(httpContext)} não pode ser nulo");
            }
            else if (db == null)
            {
                throw new ArgumentNullException($"O parâmetro {nameof(db)} não pode ser nulo");
            }

            // Se um usuário não estiver logado no identity
            if (httpContext.User == null || !httpContext.User.Identity.IsAuthenticated)
            {
                // Retorna usuário nulo
                return null;
            }

            // Busca o email de login do usuário logado
            string loginUsuarioLogado = httpContext.User.Identity.Name;


            // Busca um TbUsuario para este usuário
            var usuarioAtual = db.Usuarios.FirstOrDefault(usuario => usuario.Login.ToUpper() == loginUsuarioLogado.ToUpper());

            return usuarioAtual;
        }




        public static string GenerateToken(Usuario user)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(AppSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.Login.ToLower().ToString()),
                    new Claim(ClaimTypes.Role, user.Papel.ToString()),
                    new Claim(ClaimTypes.PrimarySid, user.Id.ToString())

                }),
                Expires = DateTime.UtcNow.AddHours(2),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }




















    }
}
