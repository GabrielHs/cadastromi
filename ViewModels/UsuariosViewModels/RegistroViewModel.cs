﻿using cadastromi.Controllers;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace cadastromi.ViewModels.UsuariosViewModels
{
    public class RegistroViewModel
    {


        [Required(ErrorMessage = "Campo obrigatorio")]
        public string Login { get; set; }

        [Required(ErrorMessage = "Campo obrigatorio")]
        public string Senha { get; set; }

        public string Papel { get; set; }





        public JsonResult ValidaDados(CadastroMiController cd)
        {


            var usuario = cd.Db.Usuarios.Where(u => u.Login.ToLower() == Login.ToLower()).Any();

            if (usuario)
            {
                cd.ModelState.AddModelError(nameof(Login), "Desculpe usuário já existe");
            }





            return null;
        }

    }
}
