﻿using cadastromi.Controllers;
using cadastromi.Models.entidades;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace cadastromi.ViewModels.UsuariosViewModels
{
    public class LoginViewModel
    {

        [Required(ErrorMessage = "Informe seu login")]
        public string Login { get; set; }

        [Required(ErrorMessage = "Informe sua senha")]
        public string Senha { get; set; }







        public JsonResult ValidaDados(CadastroMiController cd)
        {

            var usuario = cd.Db.Usuarios.FirstOrDefault(u => u.Login == Login.ToLower().Trim());

            if (usuario != null)
            {
                var passwordHasher = new PasswordHasher<Usuario>();

                var hashedPassword = passwordHasher.VerifyHashedPassword(usuario, usuario.Senha, Senha);

                if (hashedPassword != PasswordVerificationResult.Success)
                {
                    cd.ModelState.AddModelError(nameof(Senha), "Senha está incorreto");
                }


            }
            else
            {
                cd.ModelState.AddModelError(nameof(Login), "Desculpe usuário não existe, realize seu cadastro");
            }







            return null;
        }



    }
}
