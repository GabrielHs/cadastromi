﻿using cadastromi.Controllers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace cadastromi.ViewModels.ProdutosViewModels
{
    public class EnviaArquivoViewModel
    {
        [Required(ErrorMessage = "Informe o arquivo")]
        public IFormFile Arquivo { get; set; }

        public List<ProdutosInformados> ProdutosInformado { get; set; } = new List<ProdutosInformados>();


        public class ProdutosInformados
        {


            public string DataEntrega { get; set; }

            public string Nome { get; set; }

            public string Quantidade { get; set; }

            public string ValorUnitario { get; set; }

            public string DataImportacao { get; set; }

        }
      



    }
}
