﻿using cadastromi.Models.entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace cadastromi.Models.entidades
{
    public class Produto
    {
        public int Id { get; set; }

        public DateTime? DataEntrega { get; set; }
        
        public string Nome { get; set; }

        public int Quantidade { get; set; }

        public decimal ValorUnitario { get; set; }

        public DateTime DataImportacao { get; set; } = DateTime.UtcNow;

        public int UserId { get; set; }

        public Usuario Usuario { get; set; }



    }
}
