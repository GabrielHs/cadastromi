﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace cadastromi.Models.entidades
{
    public class Usuario
    {
        public int Id { get; set; }

        public string Login { get; set; }

        [JsonIgnore]
        public string Senha { get; set; }

        public string Papel { get; set; }

        public ICollection<Produto> Produtos { get; set; }



    }
}
