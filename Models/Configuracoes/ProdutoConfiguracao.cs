﻿using cadastromi.Models.entidades;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace cadastromi.Models.Configuracoes
{
    public class ProdutoConfiguracao : IEntityTypeConfiguration<Produto>
    {
        public void Configure(EntityTypeBuilder<Produto> builder)
        {
            builder.HasKey(a => a.Id);

            builder.Property(a => a.Nome).IsRequired().HasMaxLength(50);

            builder.Property(a => a.Quantidade).HasDefaultValue(1);

            builder.Property(a => a.Quantidade).IsRequired().HasColumnType("decimal(10,2)");


            builder.HasOne(a => a.Usuario)
                .WithMany(a => a.Produtos)
                .HasForeignKey(a => a.UserId)
                .OnDelete(DeleteBehavior.Cascade);



        }
    }
}
