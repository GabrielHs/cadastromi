﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace cadastromi.Migrations
{
    public partial class adddataImportacao : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "DataImportacao",
                table: "Produtos",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DataImportacao",
                table: "Produtos");
        }
    }
}
